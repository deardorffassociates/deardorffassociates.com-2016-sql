﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert ArticleTagMap
-- =============================================
CREATE PROCEDURE [Blog].[sp_Insert_ArticleTagMap]
@Article_Id int
,@Tag_Id int

AS

BEGIN TRY
   BEGIN TRANSACTION    


	INSERT INTO [Blog].[ArticleTagMap]
           ([Article_Id]
           ,[Tag_Id])
     VALUES
           (@Article_Id
           ,@Tag_Id)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO