﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert Publisher
-- =============================================
CREATE PROCEDURE [Blog].[sp_Insert_Publisher]
@Name nvarchar(50)
,@Title nvarchar(50)
,@Photo nvarchar(500)
,@UrlSlug nvarchar(50)

AS

BEGIN TRY
   BEGIN TRANSACTION    
   
		INSERT INTO [Blog].[Publisher]
			([Name]
			,[Title]
			,[Photo]
			,[UrlSlug])
		VALUES
			(@Name
			,@Title
			,@Photo
			,@UrlSlug)

	COMMIT 
		
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO