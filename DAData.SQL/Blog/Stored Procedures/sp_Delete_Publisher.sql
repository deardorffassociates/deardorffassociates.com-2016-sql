﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	1/9/2016
-- Description:	Delete Publisher
-- =============================================
CREATE PROCEDURE [Blog].[sp_Delete_Publisher]
@Id int

AS

BEGIN TRY
   BEGIN TRANSACTION    
   
	DELETE FROM [Blog].[Publisher]
	WHERE [Id] = @Id

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
