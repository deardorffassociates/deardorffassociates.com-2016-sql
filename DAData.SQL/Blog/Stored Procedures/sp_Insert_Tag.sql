﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	1/9/2016
-- Description:	Insert Tag
-- =============================================
CREATE PROCEDURE [Blog].[sp_Insert_Tag]
@Name nvarchar(50)
,@UrlSlug nvarchar(50)
,@Description nvarchar(200)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [Blog].[Tag]
           ([Name]
           ,[UrlSlug]
           ,[Description])
     VALUES
           (@Name
           ,@UrlSlug
           ,@Description)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO