﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	1/9/2016
-- Description:	Update Publisher
-- =============================================
CREATE PROCEDURE [Blog].[sp_Update_Publisher]
@Id int
,@Name nvarchar(50)
,@Title nvarchar(50)
,@Photo nvarchar(500)
,@UrlSlug nvarchar(50)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	UPDATE [Blog].[Publisher]
	   SET [Name] = @Name
		  ,[Title] = @Title
		  ,[Photo] = @Photo
		  ,[UrlSlug] = @UrlSlug
	 WHERE [Id] = @Id

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO