﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert Article
-- =============================================
CREATE PROCEDURE [Blog].[sp_Insert_Article]
@Title nvarchar(500)
,@ShortDescription nvarchar(max)
,@Description nvarchar(max)
,@Meta nvarchar(1000)
,@UrlSlug nvarchar(200)
,@Photo nvarchar(500)
,@Published bit
,@PublishedOn datetime = null
,@Category int
,@Publisher int
,@HeaderPhotoLG nvarchar(500)
,@HeaderPhotoMD nvarchar(500)
,@HeaderPhotoSM nvarchar(500)
,@HeaderPhotoXS nvarchar(500)
,@ArticleId int OUTPUT

AS

BEGIN TRY
   BEGIN TRANSACTION    

   DECLARE @createDate datetime = GETDATE()
		INSERT INTO [Blog].[Article]
           ([Title]
           ,[ShortDescription]
           ,[Description]
           ,[Meta]
           ,[UrlSlug]
		   ,[Photo]
           ,[Published]
           ,[PublishedOn]
           ,[LastUpdateDate]
		   ,[CreateDate]
           ,[Category]
		   ,[Publisher]
		   ,[HeaderPhotoLG]
		   ,[HeaderPhotoMD]
		   ,[HeaderPhotoSM]
		   ,[HeaderPhotoXS])
		VALUES
           (@Title
           ,@ShortDescription
           ,@Description
           ,@Meta
           ,@UrlSlug
		   ,@Photo
           ,@Published
           ,@PublishedOn
           ,@createDate
		   ,@createDate
           ,@Category
		   ,@Publisher
		  ,@HeaderPhotoLG
		  ,@HeaderPhotoMD
		  ,@HeaderPhotoSM
		  ,@HeaderPhotoXS)

	COMMIT 

	
	SELECT @ArticleId = IDENT_CURRENT('[Blog].[Article]') 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
