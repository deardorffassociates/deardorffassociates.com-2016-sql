﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	1/9/2016
-- Description:	Update Article
-- =============================================
CREATE PROCEDURE [Blog].[sp_Update_Article]
@Id int
,@Title nvarchar(500)
,@ShortDescription nvarchar(max)
,@Description nvarchar(max)
,@Meta nvarchar(1000)
,@UrlSlug nvarchar(200)
,@Photo nvarchar(500)
,@Published bit
,@PublishedOn datetime = null
,@Category int
,@Publisher int
,@HeaderPhotoLG nvarchar(500)
,@HeaderPhotoMD nvarchar(500)
,@HeaderPhotoSM nvarchar(500)
,@HeaderPhotoXS nvarchar(500)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	UPDATE [Blog].[Article]
	   SET [Title] = @Title
		  ,[ShortDescription] = @ShortDescription
		  ,[Description] = @Description
		  ,[Meta] = @Meta
		  ,[UrlSlug] = @UrlSlug
		  ,[Photo] = @Photo
		  ,[Published] = @Published
		  ,[PublishedOn] = @PublishedOn
		  ,[LastUpdateDate] = GETDATE()
		  ,[Category] = @Category
		  ,[Publisher] = @Publisher
		  ,[HeaderPhotoLG] = @HeaderPhotoLG
		  ,[HeaderPhotoMD] = @HeaderPhotoMD
		  ,[HeaderPhotoSM] = @HeaderPhotoSM
		  ,[HeaderPhotoXS] = @HeaderPhotoXS
	 WHERE [Id] = @Id

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
