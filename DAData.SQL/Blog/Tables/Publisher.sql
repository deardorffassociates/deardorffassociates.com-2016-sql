﻿CREATE TABLE [Blog].[Publisher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Photo] [nvarchar](500) NOT NULL,
	[UrlSlug] [nvarchar](500) NOT NULL,
	CONSTRAINT [PK_Blog.Publisher] PRIMARY KEY CLUSTERED ([Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] 
GO
