﻿
CREATE TABLE [Blog].[Article](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[ShortDescription] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Meta] [nvarchar](1000) NOT NULL,
	[UrlSlug] [nvarchar](200) NOT NULL,
	[Photo] [nvarchar](500) NOT NULL,
	[Published] [bit] NOT NULL,
	[Category] [int] NOT NULL,
	[Publisher] [int] NOT NULL,
	[PublishedOn] [datetime] NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[HeaderPhotoLG] [nvarchar](500) NOT NULL,
	[HeaderPhotoMD] [nvarchar](500) NOT NULL,
	[HeaderPhotoSM] [nvarchar](500) NOT NULL,
	[HeaderPhotoXS] [nvarchar](500) NOT NULL,
	CONSTRAINT [PK_Blog.Article] PRIMARY KEY CLUSTERED ([Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


ALTER TABLE [Blog].[Article] ADD CONSTRAINT [DF_Blog.Article_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO
ALTER TABLE [Blog].[Article] ADD CONSTRAINT [DF_Blog.Article_LastUpdateDate] DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO


ALTER TABLE [Blog].[Article] ADD  CONSTRAINT [FK_Blog.Article_Category] FOREIGN KEY([Category]) REFERENCES [Blog].[Category] ([Id])
GO
ALTER TABLE [Blog].[Article] CHECK CONSTRAINT [FK_Blog.Article_Category]
GO

ALTER TABLE [Blog].[Article] ADD  CONSTRAINT [FK_Blog.Article_Publisher] FOREIGN KEY([Publisher]) REFERENCES [Blog].[Publisher] ([Id])
GO
ALTER TABLE [Blog].[Article] CHECK CONSTRAINT [FK_Blog.Article_Publisher]
GO