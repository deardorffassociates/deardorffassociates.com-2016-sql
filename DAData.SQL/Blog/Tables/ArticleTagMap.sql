﻿CREATE TABLE [Blog].[ArticleTagMap](
	[Article_id] [int] NOT NULL,
	[Tag_id] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [Blog].[ArticleTagMap] ADD  CONSTRAINT [FK_Blog.ArticleTagMap_Article_Article_id] FOREIGN KEY([Article_id]) REFERENCES [Blog].[Article] ([Id])
GO
ALTER TABLE [Blog].[ArticleTagMap] CHECK CONSTRAINT [FK_Blog.ArticleTagMap_Article_Article_id]
GO

ALTER TABLE [Blog].[ArticleTagMap] ADD  CONSTRAINT [FK_Blog.ArticleTagMap_Tag_Tag_id] FOREIGN KEY([Tag_id]) REFERENCES [Blog].[Tag] ([Id])
GO
ALTER TABLE [Blog].[ArticleTagMap] CHECK CONSTRAINT [FK_Blog.ArticleTagMap_Tag_Tag_id]
GO
