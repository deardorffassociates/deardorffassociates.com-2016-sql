﻿

-- =============================================
-- Author:			David Schrenker
-- Create date:	02/28/2017
-- Description:	Insert Client File Submission
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_ClientFileSubmission]
@Name nvarchar(500)
,@EmailAddress nvarchar(500)
,@Comments nvarchar(4000)
,@SubmissionId int OUTPUT

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [Content].[ClientFileSubmission]
           ([Name]
           ,[EmailAddress]
           ,[Comments]
		   )
     VALUES
           (@Name
           ,@EmailAddress
           ,@Comments)

	COMMIT 
		
	SELECT @SubmissionId = IDENT_CURRENT('[Content].[ClientFileSubmission]') 
	

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
