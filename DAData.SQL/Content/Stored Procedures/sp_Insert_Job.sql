﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	10/5/2016
-- Description:	Insert Job
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_Job]
@Active bit
,@Title nvarchar(max)
,@Permalink nvarchar(500)
,@Content nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [Content].[Jobs]
           ([Active]
		   ,[Title]
		   ,[Permalink]
           ,[Content])
     VALUES
           (@Active
		   ,@Title
		   ,@Permalink
           ,@Content)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
