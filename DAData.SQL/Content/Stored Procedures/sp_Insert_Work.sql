﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	10/5/2016
-- Description:	Insert Work
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_Work]
@Name nvarchar(500)
,@Permalink nvarchar(500)
,@Active bit
,@Logo nvarchar(500)
,@WorkLogo nvarchar(500)
,@FeaturedLogo nvarchar(500)
,@Featured bit
,@FeaturedBanner nvarchar(500)
,@FeaturedBannerMD nvarchar(500)
,@FeaturedBannerSM nvarchar(500)
,@FeaturedBannerXS nvarchar(500)
,@FeaturedHeader nvarchar(500)
,@CaseStudyContent nvarchar(max)
,@CaseStudyVisible bit

AS

BEGIN TRY
   BEGIN TRANSACTION    


	INSERT INTO [Content].[Work]
           ([Name]
		   ,[Permalink]
           ,[Active]
           ,[Logo]
           ,[WorkLogo]
           ,[FeaturedLogo]
           ,[Featured]
           ,[FeaturedBanner]
           ,[FeaturedBannerMD]
           ,[FeaturedBannerSM]
           ,[FeaturedBannerXS]
           ,[FeaturedHeader]
           ,[CaseStudyContent]
           ,[CaseStudyVisible])
     VALUES
           (@Name
		   ,@Permalink
			,@Active
			,@Logo
			,@WorkLogo
			,@FeaturedLogo
			,@Featured
			,@FeaturedBanner
			,@FeaturedBannerMD
			,@FeaturedBannerSM
			,@FeaturedBannerXS
			,@FeaturedHeader
			,@CaseStudyContent
			,@CaseStudyVisible
			)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO