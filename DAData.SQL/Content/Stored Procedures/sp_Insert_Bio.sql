﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert Leadership Bio
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_Bio]
@Name nvarchar(500)
,@Title nvarchar(500)
,@Active bit
,@LargePhoto nvarchar(500)
,@SmallPhoto nvarchar(500)
,@Bio nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    


	INSERT INTO [Content].[Bio]
           ([Name]
           ,[Title]
           ,[Active]
           ,[LargePhoto]
           ,[SmallPhoto]
           ,[Bio])
     VALUES
           (@Name
           ,@Title
           ,@Active
           ,@LargePhoto
           ,@SmallPhoto
           ,@Bio)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
