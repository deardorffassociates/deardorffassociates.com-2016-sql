﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	10/5/2016
-- Description:	Update Job
-- =============================================
CREATE PROCEDURE [Content].[sp_Update_Job]
@JobId int
,@Active bit
,@Title nvarchar(500)
,@Permalink nvarchar(500)
,@Content nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	UPDATE [Content].[Jobs]
	   SET 
		[Active] = @Active
		,[Title] = @Title
		,[Permalink] = @Permalink
		,[Content] = @Content
		,[LastUpdateDate] = GETUTCDATE()
	 WHERE [Id] = @JobId

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
