﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/5/2016
-- Description:	Delete Work
-- =============================================
CREATE PROCEDURE [Content].[sp_Delete_Work]
@WorkId int

AS

BEGIN TRY
   BEGIN TRANSACTION    
   
	DELETE FROM [Content].[Work]
	WHERE [Id] = @WorkId

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO