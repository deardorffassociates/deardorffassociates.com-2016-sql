﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	10/5/2016
-- Description:	Update Leadership Bio
-- =============================================
CREATE PROCEDURE [Content].[sp_Update_Bio]
@Id int
,@Name nvarchar(500)
,@Title nvarchar(500)
,@Active bit
,@LargePhoto nvarchar(500)
,@SmallPhoto nvarchar(500)
,@Bio nvarchar(max)


AS

BEGIN TRY
   BEGIN TRANSACTION    


	UPDATE [Content].[Bio]
	   SET [Name] = @Name
		  ,[Title] = @Title
		  ,[Active] = @Active
		  ,[LargePhoto] = @LargePhoto
		  ,[SmallPhoto] = @SmallPhoto
		  ,[Bio] = @Bio
		  ,[LastUpdateDate] = GETUTCDATE()
	 WHERE [Id] = @Id

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
