﻿
-- =============================================
-- Author:			David Schrenker
-- Create date:	02/28/2017
-- Description:	Delete ClientFile
-- =============================================
CREATE PROCEDURE [Content].[sp_Delete_ClientFile]
@FileId int

AS

BEGIN TRY
   BEGIN TRANSACTION    
   
	DELETE FROM [Content].[ClientFile]
	WHERE [Id] = @FileId

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
