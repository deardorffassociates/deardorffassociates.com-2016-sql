﻿

-- =============================================
-- Author:			David Schrenker
-- Create date:	02/28/2017
-- Description:	Insert Client File
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_ClientFile]
@SubmissionId int
,@FileName nvarchar(500)
,@OrigianlFileName nvarchar(500)
,@FileSize int
,@FileId int OUTPUT

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [Content].[ClientFile]
           ([SubmissionId]
           ,[FileName]
           ,[OrigianlFileName]
		   ,[FileSize])
     VALUES
           (@SubmissionId
           ,@FileName
           ,@OrigianlFileName
		   ,@FileSize)

	COMMIT 
		
	SELECT @FileId = IDENT_CURRENT('[Content].[ClientFile]') 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
