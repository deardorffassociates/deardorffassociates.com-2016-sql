﻿

-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert Award
-- =============================================
CREATE PROCEDURE [Content].[sp_Insert_Awards]
@Active bit
,@Name nvarchar(500)
,@Logo nvarchar(500)
,@Content nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [Content].[Awards]
           ([Name]
           ,[Logo]
           ,[Content])
     VALUES
           (@Name
           ,@Logo
           ,@Content)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO
