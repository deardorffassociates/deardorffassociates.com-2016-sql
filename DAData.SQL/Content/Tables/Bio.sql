﻿
CREATE TABLE [Content].[Bio] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
    [Active] [bit] NOT NULL,
	[LargePhoto] [nvarchar](500) NOT NULL,
	[SmallPhoto] [nvarchar](500) NOT NULL,
	[Bio] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL
	CONSTRAINT [PK_Content.Bio] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Content].[Bio] ADD CONSTRAINT [DF_Content.Bio_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO

ALTER TABLE [Content].[Bio] ADD CONSTRAINT [DF_Content.Bio_LastUpdateDate] DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO

ALTER TABLE [Content].[Bio] ADD CONSTRAINT [DF_Content.Bio_Active] DEFAULT (1) FOR [Active]
GO
