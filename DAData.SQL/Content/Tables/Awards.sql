﻿
CREATE TABLE [Content].[Awards] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
    [Active] [bit] NOT NULL,
	[Logo] [nvarchar](500) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL
	CONSTRAINT [PK_Content.Awards] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Content].[Awards] ADD CONSTRAINT [DF_Content.Awards_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO

ALTER TABLE [Content].[Awards] ADD CONSTRAINT [DF_Content.Awards_LastUpdateDate] DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO

ALTER TABLE [Content].[Awards] ADD CONSTRAINT [DF_Content.Awards_Active] DEFAULT (1) FOR [Active]
GO