﻿CREATE TABLE [Content].[ClientFile]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SubmissionId] [int] NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[OrigianlFileName] [nvarchar](500) NOT NULL,
    [FileSize] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
	CONSTRAINT [PK_Content.ClientFile] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Content].[ClientFile] ADD CONSTRAINT [DF_Content.ClientFile_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO

ALTER TABLE [Content].[ClientFile] ADD  CONSTRAINT [FK_Content.ClientFile_ClientFileSubmission_Submission_id] FOREIGN KEY([SubmissionId]) REFERENCES [Content].[ClientFileSubmission] ([Id])
GO
ALTER TABLE [Content].[ClientFile] CHECK CONSTRAINT [FK_Content.ClientFile_ClientFileSubmission_Submission_id]
GO