﻿CREATE TABLE [Content].[ClientFileSubmission]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[EmailAddress] [nvarchar](500) NOT NULL,
	[Comments] [nvarchar](4000) NOT NULL,
	[Submitted] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL
	CONSTRAINT [PK_Content.ClientFileSubmission] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Content].[ClientFileSubmission] ADD CONSTRAINT [DF_Content.ClientFileSubmission_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO
ALTER TABLE [Content].[ClientFileSubmission] ADD CONSTRAINT [DF_Content.ClientFileSubmission_Submitted] DEFAULT (0) FOR [Submitted]
GO
