﻿
CREATE TABLE [Content].[Jobs] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[Permalink] [nvarchar](500) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL
	CONSTRAINT [PK_Content.Jobs] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Content].[Jobs] ADD CONSTRAINT [DF_Content.Jobs_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO

ALTER TABLE [Content].[Jobs] ADD CONSTRAINT [DF_Content.Jobs_LastUpdateDate] DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO
