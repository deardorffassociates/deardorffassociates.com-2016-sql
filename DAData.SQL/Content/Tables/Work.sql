﻿CREATE TABLE [Content].[Work](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Permalink] [nvarchar] (500) NOT NULL,
	[Active] [bit] NOT NULL,
	[Logo] [nvarchar] (500) NULL,
	[WorkLogo] [nvarchar] (500) NULL,
	[FeaturedLogo] [nvarchar] (500) NULL,
	[Featured] [bit] NOT NULL,
	[FeaturedBanner] [nvarchar] (500) NULL,
	[FeaturedBannerMD] [nvarchar](500) NULL,
	[FeaturedBannerSM] [nvarchar](500) NULL,
	[FeaturedBannerXS] [nvarchar](500) NULL,
	[FeaturedHeader] [nvarchar](500) NULL,
	[CaseStudyContent] [nvarchar](max) NULL,
	[CaseStudyVisible] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Content.Work] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [Content].[Work] ADD CONSTRAINT [DF_Content.Work_Active] DEFAULT ((0)) FOR [Active]
GO
ALTER TABLE [Content].[Work] ADD CONSTRAINT [DF_Content.Work_Featured] DEFAULT ((0)) FOR [Featured]
GO
ALTER TABLE [Content].[Work] ADD CONSTRAINT [DF_Content.Work_CaseStudyVisible] DEFAULT ((0)) FOR [CaseStudyVisible]
GO
ALTER TABLE [Content].[Work] ADD CONSTRAINT [DF_Content.Work_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO
ALTER TABLE [Content].[Work] ADD CONSTRAINT [DF_Content.Work_LastUpdateDate] DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO
