﻿
CREATE TABLE [General].[ContactUs] (
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[EmailAddress] [nvarchar](500) NOT NULL,
	[Phone] [nvarchar](500) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL
	CONSTRAINT [PK_General.ContactUs] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [General].[ContactUs] ADD CONSTRAINT [DF_General.ContactUs_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO