﻿CREATE TABLE [General].[ErrorLog] (
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceName] [nvarchar](500) NOT NULL,
	[EntryType] [smallint] NOT NULL,
	[EventId] [int] NOT NULL,
	[MachineName] [nvarchar](500) NOT NULL,
	[Username] [nvarchar](500) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL
	CONSTRAINT [PK_General.ErrorLog] PRIMARY KEY CLUSTERED (	[Id] ASC)
WITH (PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [General].[ErrorLog] ADD CONSTRAINT [DF_General.ErrorLog_CreateDate] DEFAULT (getutcdate()) FOR [CreateDate]
GO
