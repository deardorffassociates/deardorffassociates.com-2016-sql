﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert ErrorLog Entry
-- =============================================
CREATE PROCEDURE [General].[sp_Insert_ContactUs]
@Name nvarchar(500)
,@EmailAddress nvarchar(500)
,@Phone nvarchar(500)
,@Message nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [General].[ContactUs]
           ([Name]
           ,[EmailAddress]
           ,[Phone]           
           ,[Message])
     VALUES
           (@Name
           ,@EmailAddress
           ,@Phone           
           ,@Message)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO