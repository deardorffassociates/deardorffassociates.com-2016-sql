﻿-- =============================================
-- Author:			David Schrenker
-- Create date:	10/10/2016
-- Description:	Insert ErrorLog Entry
-- =============================================
CREATE PROCEDURE [General].[sp_Insert_ErrorLogEntry]
@SourceName nvarchar(500)
,@EntryType smallint
,@EventId int
,@MachineName nvarchar(500)
,@Username nvarchar(500)
,@Message nvarchar(max)

AS

BEGIN TRY
   BEGIN TRANSACTION    

	INSERT INTO [General].[ErrorLog]
           ([SourceName]
           ,[EntryType]
           ,[EventId]
           ,[MachineName]
           ,[Username]
           ,[Message])
     VALUES
           (@SourceName
           ,@EntryType
           ,@EventId
           ,@MachineName
           ,@Username
           ,@Message)

	COMMIT 
	
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0 ROLLBACK
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT @ErrMsg = 'Message: '+ ERROR_MESSAGE() + ', Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + ', Line ' + CONVERT(VARCHAR(5), ERROR_LINE());
	SELECT @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO